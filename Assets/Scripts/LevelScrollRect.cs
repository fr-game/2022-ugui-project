using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelScrollRect : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{
    private ScrollRect scroll;

    private float[] pagePosition = new float[4] { 0, 0.3333f, 0.6666f, 1 };

    public Toggle[] toggleArray;

    private float targetPosition = 0;
    private bool isMoving = false;
    private float speed = 7;

    // Start is called before the first frame update
    void Start()
    {
        scroll = GetComponent<ScrollRect>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            scroll.horizontalNormalizedPosition = Mathf.Lerp(scroll.horizontalNormalizedPosition, targetPosition, Time.deltaTime * speed);
            if (Mathf.Abs(scroll.horizontalNormalizedPosition - targetPosition) < 0.001f)
            {
                isMoving = false;
                scroll.horizontalNormalizedPosition = targetPosition;
            }
        }
    }
    public void OnValueChange(Vector2 v)
    {
        print(v);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        float currentPosition = scroll.horizontalNormalizedPosition;
        int index = 0;
        float offset = currentPosition - pagePosition[0];
        for (int i = 1; i < 4; i++)
        {
            if (Mathf.Abs(currentPosition - pagePosition[i]) < offset)
            {
                index = i;
                offset = Mathf.Abs(currentPosition - pagePosition[i]);
            }
        }

        targetPosition = pagePosition[index];
        isMoving = true;
        toggleArray[index].isOn = true;
    }

    public void MoveToPage1(bool isOn)
    {
        if (isOn)
        {
            isMoving = true;
            targetPosition = pagePosition[0];
        }
    }
    public void MoveToPage2(bool isOn)
    {
        if (isOn)
        {
            isMoving = true;
            targetPosition = pagePosition[1];
        }
    }
    public void MoveToPage3(bool isOn)
    {
        if (isOn)
        {
            isMoving = true;
            targetPosition = pagePosition[2];
        }
    }
    public void MoveToPage4(bool isOn)
    {
        if (isOn)
        {
            isMoving = true;
            targetPosition = pagePosition[3];
        }
    }
}
